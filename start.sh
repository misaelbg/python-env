#!/bin/bash

# aws sync...
/opt/conda/bin/aws s3 sync /app/data/ # *****

echo $AWS_DEFAULT_REGION
echo $AWS_ACCESS_KEY_ID
echo $AWS_SECRET_ACCESS_KEY

# jupyter
jupyter notebook --no-browser --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token='1234'

echo $PATH
