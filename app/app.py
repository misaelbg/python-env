import json
from flask import jsonify, Response, request
from flask_api import FlaskAPI
from flask_cors import CORS
from werkzeug.exceptions import HTTPException

app = FlaskAPI(__name__)
app.config["CORS_HEADERS"] = "Content-Type"
CORS(app, resources={r"/*": {"origins": "*"}})

@app.errorhandler(Exception)
def handle_error(e):
    code = 500
    if isinstance(e, HTTPException):
        code = e.code

    return jsonify(error=str(e), text=None), code

@app.route("/hello", methods=["POST", "OPTIONS"])
def prediction():
    data = request.get_json(silent=True)
    res = ['hello world']

    return Response(json.dumps(res), mimetype="application/json")

if __name__ == "__main__":
    app.run(host="0.0.0.0")
