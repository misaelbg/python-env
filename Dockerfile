FROM python:3.6

LABEL maintainer="Misael Borges <misa-er@hotmail.com>"

RUN apt update -y && apt install python3-dev -y

RUN wget https://repo.continuum.io/archive/Anaconda3-5.0.0-Linux-x86_64.sh && \
	bash Anaconda3-5.0.0-Linux-x86_64.sh -b && \
	ln -f /root/anaconda3/bin/conda /usr/local/bin/conda

WORKDIR /app
COPY /app /app
COPY ./requirements.txt /app
COPY ./start.sh /docker-entrypoint.sh

RUN pip install -r /app/requirements.txt

RUN conda install -c menpo opencv3 -y && \
	conda install -y -c anaconda ncurses && \
	conda install -y -c conda-forge libiconv geoplot && \
    conda upgrade dask

EXPOSE 5000
CMD ["uwsgi", "--ini", "/app/wsgi.ini"]
